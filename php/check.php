<?php

include "db.php"; // подключаем бд
include "logger.php"; // логгер

$ram = 2000; // кол-во выделенной оперативной памяти (в мб)
$log;

if ($mysqli->connect_error) { // проверка подключения к бд

    Logger::getLogger('error')->log('Ошибка при подключении бд: ' . $mysqli->connect_error);
    $log .= " Ошибка при подключении бд | ";

} else {
    $log .= " БД успешно подключена | ";
}

if (discFreeGb(__FILE__, 2) < 1) { // проверка есть ли свободное место на диске

    Logger::getLogger('critical')->log('Недостаточно места на диске');

} elseif (discFreeGb(__FILE__, 2) < 10) { // проверка есть ли 10гб свободного места на диске

    Logger::getLogger('warn')->log('Места на диске осталось менее 10гб');

}

if (ceil(memory_get_usage() / 1024) > $ram) { // проверка достаточно ли оперативной памяти

    Logger::getLogger('critical')->log('Недостаточно оперативной памяти');

} elseif (ceil(memory_get_usage() / 1024) > $ram * (80/100)) { // проверка занято ли 80% оперативной памяти

    Logger::getLogger('warn')->log('Занято более 80% оперативной памяти');

}

function discFreeGb($space, $level){

    $b = disk_free_space(dirname("$space", "$level"));
    $gb = $b / 1024 / 1024;

    return $gb;
}

$freeGb = floor(discFreeGb(__FILE__, 2));
$freeRam = 100 - floor(100 / ($ram / (memory_get_usage() / 1024)));
$log .= "Кол-во свободного места на диске = $freeGb МБ | ";
$log .= "Свободного места в оперативной памяти = $freeRam%";

Logger::getLogger('log')->log("$log"); // введение логов
