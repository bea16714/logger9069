<?php

function readLog(){
    $q = dirname(__FILE__,2) . "\log\log.log";
    $fp = fopen($q, 'r');

    if ($fp)
    {
        while (!feof($fp))
        {
            $mytext = fgets($fp, 999);
            echo $mytext."<br />";
        }
    }
    else echo "Ошибка при открытии файла";
    fclose($fp);
}

function readWarn(){
    $q = dirname(__FILE__,2) . "\log\warn.log";
    $fp = fopen($q, 'r');

    if ($fp)
    {
        while (!feof($fp))
        {
            $mytext = fgets($fp, 999);
            echo $mytext."<br />";
        }
    }
    else echo "Ошибка при открытии файла";
    fclose($fp);
}

function readError(){
    $q = dirname(__FILE__,2) . '\log\error.log';
    $fp = fopen($q, 'r');

    if ($fp)
    {
        while (!feof($fp))
        {
            $mytext = fgets($fp, 999);
            echo $mytext."<br />";
        }
    }
    else echo "Ошибка при открытии файла";
    fclose($fp);
}

function readCritical(){
    $q = dirname(__FILE__,2) . '\log\critical.log';
    $fp = fopen($q, 'r');

    if ($fp)
    {
        while (!feof($fp))
        {
            $mytext = fgets($fp, 999);
            echo $mytext."<br />";
        }
    }
    else echo "Ошибка при открытии файла";
    fclose($fp);
}


