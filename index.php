<?php
include "php/check.php";
include "php/read.php";
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Логгер</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>
<body>
    <header>
        </br>
    </header>

    <div class="container">
        <div class="critical">
            <h2>critical</h2>
            <p style="color: #FF0000"><?readCritical()?></p>
        </div>
        </br>
        <div class="error">
            <h2>error</h2>
            <p style="color: #800000"><?readError()?></p>
        </div>
        </br>
        <div class="warn">
            <h2>warn</h2>
            <p style="color: #8B4513"><?readWarn()?></p>
        </div>
        </br>
        <div class="log">
            <h2>log</h2>
            <p><?readLog()?></p>
        </div>

    </div>

    <footer>

    </footer>
</body>
</html>

